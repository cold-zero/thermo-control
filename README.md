# Thermo Control

Control your central heating using an ESP8266 or ESP32 and a relay.  

This device will serve as a AP gateway for the thermal control system in the home automation. This device will control the central heating device using a realy.

## API
Default ip address of the device should be 192.168.4.1  
To control the device you can either use the built in interface via the browser or via api calls.  
To turn on the central heating call ip/on  
TO turn off the central heating call ip/off  

## Data files
The data files should be placed in the "data" folder. To upload the data files you have to put the controller in flash mode by holding FLASH button and pressing once RESET while holding the FLASH button. After that run the following command in a platformio terminal

```
pio run -t uploadfs
```