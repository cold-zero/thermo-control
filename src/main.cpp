// Import required libraries
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <Hash.h>
#include <FS.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>

const char *ssid = "Thermo-Control";
const char *password = "anaar3m3r3";

const int relayPin = 5;
String relayState;

String hostname = "thermo-control.local";

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis = 0; // will store last time DHT was updated
const long interval = 10000;

String processor(const String& var) 
{
    if (var == "THERMOSTATE") {
        if (digitalRead(relayPin)) 
        {
            relayState = "OFF";
        } 
        else
        {
            relayState = "ON";
        }

        return relayState;
    }

    return "N/A";
}

String getState()
{
    if (digitalRead(relayPin))
    {
        relayState = "OFF";
    }
    else
    {
        relayState = "ON";
    }
    
    return relayState;
}

void notFound(AsyncWebServerRequest *request)
{
    request->send(404, "text/plain", "Not found");
}


void setup()
{
    // Serial port for debugging purposes
    Serial.begin(115200);

    pinMode(relayPin, OUTPUT);
    digitalWrite(relayPin, HIGH); // It's a low level trigger relay, so OFF = HIGH

    if (!SPIFFS.begin()) {
        Serial.println("An error has occured while mounting SPIFFS");
        return;
    }

    Serial.println("Setting AP (Access Point)...");
    // Remove the password parameter, if you want the AP (Access Point) to be open
    WiFi.softAP(ssid, password);

    WiFi.config(INADDR_NONE, INADDR_NONE, INADDR_NONE, INADDR_NONE);
    WiFi.setHostname(hostname.c_str());

    IPAddress IP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(IP);

    // Print ESP8266 Local IP Address
    Serial.println(WiFi.localIP());

    // Route for root / web page
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(SPIFFS, "/index.html", String(), false, processor);
        Serial.println("Root accessed!");
    });
    server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(SPIFFS, "/style.css", "text/css");
    });

    server.on("/on", HTTP_GET, [](AsyncWebServerRequest *request) {
        if (request->hasParam("webview"))
        {
            int isWebview = request->getParam("webview")->value().toInt();
            if (isWebview)
            {
                digitalWrite(relayPin, LOW);
                request->send(SPIFFS, "/index.html", String(), false, processor);
                Serial.println("Heater on command recieved from webview.");
            }
        }
        else /* no webview */
        {
            digitalWrite(relayPin, LOW);
            request->send_P(200, "text/plain", getState().c_str());
            Serial.println("Heater on command received!");
        }
    });
    server.on("/off", HTTP_GET, [](AsyncWebServerRequest *request) {
        if (request->hasParam("webview"))
        {
            int isWebview = request->getParam("webview")->value().toInt();
            if (isWebview)
            {
                digitalWrite(relayPin, HIGH);
                request->send(SPIFFS, "/index.html", String(), false, processor);
                Serial.println("Heater off command recieved from webview.");
            }
        }
        else /* no webview */
        {
            digitalWrite(relayPin, HIGH);
            request->send_P(200, "text/plain", getState().c_str());
            Serial.println("Heater off command received!");
        }
    });

    server.on("/state", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send_P(200, "text/plain", getState().c_str());
    });

    server.onNotFound(notFound);

    // Start server
    server.begin();
}

void loop()
{
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= interval)
    {
        // save the last time you updated the DHT values
        previousMillis = currentMillis;

        // Main loop shit here
    }
}